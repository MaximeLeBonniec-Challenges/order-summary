# Frontend Mentor - Order summary card solution

This is a solution to the [Order summary card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/order-summary-component-QlPmajDUj). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- See hover states for interactive elements

### Screenshot

Screenshot of my solution :

![Screenshot of my integration](./design/screen_of_my_integration.png)

### Links

- Live Site URL: [https://maximelebonniec-challenges.gitlab.io/order-summary/](https://maximelebonniec-challenges.gitlab.io/order-summary/)

## My process

### Built with

- HTML5
- CSS 3
- Flexbox
- CSS variables

### What I learned

To use ...

- Flexbox
- Variables
- Transitions
- Box shadow

I also learned to put an image in the background that takes up the whole screen.

### Continued development

I want perfect me in the use of flexbox. In this project, it was juste a little introduction.

Also, element sizing and placement are still techniques I need to improve.

## Author

- Website - [Maxime Le Bonniec](https://gitlab.com/MaximeLeBonniec/)
- Frontend Mentor - [@MaximeLeBonniec](https://www.frontendmentor.io/profile/MaximeLeBonniec)
- LinkedIn - [@MaximeLeBonniec](https://www.linkedin.com/in/maxime-le-bonniec/)